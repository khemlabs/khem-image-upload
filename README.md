# README

Khemlabs - Kickstarter - Khem log

### What is this repository for?

This lib is intended for developers that are using the khemlabs kickstarter framework. It manages the file upload of
images.

### Configure parameters

* file: req.file.path (upload file path)
* path: all image formats src destintation path
* destination: raw image format src destintation path
* validTypes: (optional) image valid type defaults ['png', 'jpeg', 'jpg', 'gif'])

### Howto

#### Send image

```nodejs
const express = require('express'),
	router = express.Router(),
	multer = require('multer'),
	upload = multer({ dest: __config.PATH_UPLOAD }),
	kimage = require('khem-image-upload'),
	kauth = require('khem-auth');

router.get('/:id/:type?', (req, res, next) => {
	// Send the image
	const image = kimage.configure();
	return image.send(req, res);
});

module.exports = router;
```

#### Upload image

```nodejs
const express = require('express'),
	router = express.Router(),
	multer = require('multer'),
	upload = multer({ dest: __config.PATH_UPLOAD }),
	kimage = require('khem-image-upload'),
	kauth = require('khem-auth');

router.post('/', kauth.authenticate(['admin']), upload.single('image'), (req, res, next) => {
	// If the user has uploaded a file
	if (req.file && req.file.path) {
		const file = req.file.path;
		const path = `${__config.PATH_IMG}${req.file.filename}`;
		const destination = `${path}/raw`;
		// Configure the image object, create the file and return the id
		const image = kimage.configure(file, path, destination);
		//Create path, move image to path, resize image and return the id
		// Send the id of the image
		return image.create(err => image.sendID(err, req, res));
	}
	// If no file uploaded
	return res.json({ success: false });
});

module.exports = router;
```

#### Delete image

```nodejs
const express = require('express'),
	router = express.Router(),
	multer = require('multer'),
	upload = multer({ dest: __config.PATH_UPLOAD }),
	kimage = require('khem-image-upload'),
	kauth = require('khem-auth');

router.delete('/:id', (req, res, next) => {
	// Send the image
	const image = kimage.configure();
	return image.delete(req, res);
});

module.exports = router;
```

### Requirements

> Khemlabs kickstarter server

### Who do I talk to?

* dnangelus repo owner and admin
* developer elgambet and khemlabs
