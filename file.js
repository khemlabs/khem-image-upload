const fs = require('fs'),
	log = require('khem-log');

/**
 * Creates a directory
 * @param {string} path
 */
exports.createDir = function(path, next) {
	//Create uploaded image directory (hash)
	fs.mkdir(path, err => {
		if (err) return next(err);
		log.info('FS: File path created. OK', 'khem-image-upload/file', 'createDir');
		return next();
	});
};

/**
 * Moves a file from the source path to de destination
 * @param {string} source
 * @param {string} destination
 * @param {function} next
 */
exports.move = function(source, destination, next) {
	fs.rename(source, destination, (err, data) => {
		if (err) return next(err);
		log.info('FS: Moved file from tmp. OK', 'khem-image-upload/file', 'move');
		return next();
	});
};

/**
 * Unlinks a file
 * @param {path} path to unlink
 * @param {function} next
 */
exports.unlink = function(path, next) {
	try {
		fs.unlink(path);
		return next();
	} catch (err) {
		return next(err);
	}
};

exports.send = function sendImage(path, type, res) {
	fs.readFile(path, (err, data) => {
		if (err) {
			log.error(err, 'khem-image-upload/file', 'send');
			return res.status(404).send('');
		}
		res.writeHead(200, { 'Content-Type': type });
		return res.end(data, 'binary');
	});
};
