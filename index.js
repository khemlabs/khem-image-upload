const fileService = require('./file'),
	gm = require('gm').subClass({ imageMagick: true }),
	async = require('async'),
	rimraf = require('rimraf'),
	log = require('khem-log');

const imageHelper = function(file, path, destination, validTypes = ['png', 'jpeg', 'jpg', 'gif']) {
	this.validTypes = validTypes;

	this.file = file;

	this.path = path;

	this.destination = destination;

	this.defaults = {
		type: 'square',
		content: 'image/png'
	};

	this.resizes = [
		{ width: 400, height: 215, name: 'wide' },
		{ width: 240, height: 240, name: 'square' },
		{ width: 50, height: 50, name: 'small' }
	];

	this.createPath = next => fileService.createDir(this.path, next);
	this.move = next => fileService.move(this.file, this.destination, next);
	this.unlink = next => fileService.unlink(this.destination, next);

	this.validFormat = next => {
		gm(this.file).format((err, value) => {
			if (err) return next(err);
			if (this.validTypes.indexOf(value) >= 0) return next(`format ${value} is not supported`);
			return next();
		});
	};

	/**
	 * Rezise a image
	 * @param {object} image
	 * @param {string} destination base path
	 * @param {string} image path
	 * @param {array} sizes {width: {number}, height: {number}, name: {string}}
	 * @param {function} next
	 */
	this.resize = next => {
		async.each(
			this.resizes,
			(size, nextSize) => {
				gm(this.destination)
					.resize(size.width, size.height, '!')
					.write(this.path + '/' + size.name, err => {
						if (!err) {
							log.info('GM: image ' + size.name + ' saved. OK', 'khem-image-upload/index', 'resizeImage');
						}
						return nextSize(err);
					});
			},
			err => next(err)
		);
	};

	this.create = callback => {
		async.waterfall([this.validFormat, this.createPath, this.move, this.resize, this.unlink], err => callback(err));
	};

	this.delete = (req, res, cb) => {
		const path = `${__config.PATH_IMG}${req.params.id}/`;
		return rimraf(path, () => {
			return cb ? cb() : res.status(204).send('');
		});
	};

	this.send = (req, res) => {
		const base = __config.PATH_IMG + req.params.id;
		const type = req.params.type || this.defaults.type;
		const path = base + '/' + type;
		return fileService.send(path, this.defaults.content, res);
	};

	this.sendID = (err, req, res) => {
		if (err) {
			log.error(err, 'khem-image-upload/index', 'sendID');
			return res.status(500).send(err);
		}
		return res.json({ id: req.file.filename });
	};
};

exports.configure = function(file, path, destination) {
	return new imageHelper(file, path, destination);
};
